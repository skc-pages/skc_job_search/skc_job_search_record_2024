

Platform Engineer ? REMOTE ? CONTRACT
InboxSearch for all messages with label InboxRemove label Inbox from this conversation
?
Kiran Konda Unsubscribe
10:14?AM (24 minutes ago)
???
to me
?
?
1:43
Hi Stephen,

Greetings !!!
 
This is Kiran from Open Systems. Our Global Client is looking for a Platform Engineer ? REMOTE ? CONTRACT. Please let me know if you are interested to discuss further.
 
Platform Engineer ? REMOTE ? CONTRACT
Only W2 ? NO C2C
 
 
This position is part of the Enterprise DevOps Shared Services team and is focused on advancing our strategy to make enterprise-grade service offerings available to development teams to improve continuous delivery of applications by applying DevOps best practices, processes, and tools across the organization. This position will provide leadership around architecture and design decisions, capability delivery and innovation of the Enterprise DevOps products and service offerings.

This role will be responsible for partnering with Enterprise Architecture, Cloud CoE and Platform teams to drive, and execute an application and developer experience-first strategy and roadmap for the destination state of the Enterprise DevOps organization. This role will help define, build, guide, and oversee the strategy and implementation of DevOps and platform engineering across the enterprise.

Role Description:
The Platform Engineer understands engineering needs including those required to build, deploy, and operate a platform through all phases of its software delivery lifecycle and is focused on improving developer productivity, streamlining the current onboarding experience, and overall SDLC process that supports modern service and application engineering from idea to production.

The Expertise we?re looking for
Demonstrable experience enabling developers and development teams with self-service capabilities
Demonstrable experience in Cloud Architecture and/or migration skills
Experience in AWS services technologies and practices, building applications using services such as AWS EKS, ECS, Lambda, EC2, Cloud Formation, OpenShift
A well-grounded knowledge of engineering and continuous integration & delivery CI/CD practices.
Experience with building containers-based systems: Architecture, implementation and managing orchestration platform such as: Kubernetes, Docker, EKS, ECS.
Deep understanding of well architected framework implementation in Enterprise: Operational Excellence, Security, Reliability, Performance Efficiency and Cost optimization.
Strong understanding of Domain Driven Design and the ability to apply it to new domains.
Experience in building Event Driven systems and Architecture
Proven experience developing systems based on patterns of microservices
Iterate on existing features, and build a world-class developer experience
Basic Qualifications
Understands the environment and technologies, and their capabilities, constraints and configurations.
Security expertise to ensure that the platform enhances security posture, including the security of the platform and the security of the apps delivered with the platform.
Suggest methods and new technologies for increasing the effectiveness of changes and of general production support improvements
Experience working in a distributed, cloud-based environment using Azure/AWS/GCP (Docker/Kubernetes)
Experience with (GitHub Actions, Ansible, Terraform, Jenkins, Artifactory)
Experience in developing software using languages such as TypeScript/JavaScript
Experience with Micro Frontends and cloud-based services, preferably AWS.
Experience with modern SDLC tools, branching strategies, and ability to develop and enforce CI/CD practices
 
  
Regards
Kiran Konda
P: 469-530-4939
kkonda@opensystemstech.com
500 N. Akard St. Suite 2010 Dallas, TX 75201
www.opensystemstech.com
